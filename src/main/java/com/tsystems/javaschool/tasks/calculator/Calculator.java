package com.tsystems.javaschool.tasks.calculator;

import java.text.ParseException;
import java.util.Collections;
import java.util.Stack;
import java.util.StringTokenizer;

public class Calculator {

    private final String OPERATORS = "+-*/";

    // temporary stack that holds operators and brackets
    private Stack<String> stackOperations = new Stack<String>();

    // stack for holding expression converted to reversed polish notation
    private Stack<String> stackRPN = new Stack<String>();

    // stack for holding the calculations result
    private Stack<String> stackAnswer = new Stack<String>();

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        try {
            parse(statement);
        } catch (ParseException pe){
            return null;
        }
        return evalRPN();
    }

    /**
     * Evaluate Reverse Polish Notation
     *
     * @return result of evaluation
     */
    private String evalRPN(){
        while (!stackRPN.empty()) {
            String token = stackRPN.pop();
            if (isNumber(token)) {
                stackAnswer.push(token);
            } else if (isOperator(token)) {

                String a = stackAnswer.pop();
                String b = stackAnswer.pop();

                switch (token) {
                    case "+": {
                        a = (Double.toString(Double.valueOf(b) + Double.valueOf(a)));
                    }
                    break;
                    case "-": {
                        a = (Double.toString(Double.valueOf(b) - Double.valueOf(a)));
                    }
                    break;
                    case "*": {
                        a = (Double.toString(Double.valueOf(b) * Double.valueOf(a)));
                    }
                    break;
                    case "/": {
                        a = (Double.toString(Double.valueOf(b) / Double.valueOf(a)));
                    }
                    break;
                }
                if (isInteger(a)){
                    stackAnswer.push(a.substring(0,a.indexOf('.')));
                } else {
                    if (a.equals("Infinity")) return null;
                    stackAnswer.push(a);
                }
            }
        }
        return stackAnswer.pop();
    }

    /**
     * Transform an infix notation into the RPN
     *
     * @param expression RPN expression for evaluating
     * @throws ParseException if expression is not constructed correctly
     */
    private void parse(String expression) throws ParseException{

        if (expression == null || expression.equals("")){
            throw new ParseException("Выражение для выисления пустое!",0);
        }
        if (expression.contains("++") || expression.contains("--") || expression.contains("**") || expression.contains("//")){
            throw new ParseException("Выражение содержит лишние символы операций!",1);
        }

        stackOperations.clear();
        stackRPN.clear();

        expression = expression.replace(" ", "").replace("(-", "(0-")
                .replace(",-", ",0-");
        if (expression.charAt(0) == '-') {
            expression = "0" + expression;
        }

        StringTokenizer stringTokenizer = new StringTokenizer(expression,
                OPERATORS + "()", true);

        //main loop with Dijkstra's Shunting-yard algorithm for creating a RPN
        while (stringTokenizer.hasMoreTokens()) {
            String token = stringTokenizer.nextToken();
            if (isNumber(token)) {
                stackRPN.push(token);
            } else if (isOperator(token)) {
                while (!stackOperations.empty()
                        && isOperator(stackOperations.lastElement())
                        && getPrecedence(token) <= getPrecedence(stackOperations.lastElement())) {
                    stackRPN.push(stackOperations.pop());
                }
                stackOperations.push(token);
            } else if (isOpenBracket(token)) {
                stackOperations.push(token);
            } else if (isCloseBracket(token)) {
                while (!stackOperations.empty()
                        && !isOpenBracket(stackOperations.lastElement())) {
                    stackRPN.push(stackOperations.pop());
                }
                if (stackOperations.empty()){
                    throw new ParseException("В выражении не хватает открывающей скобки!", 2);
                } else {
                    stackOperations.pop();
                }
            } else {
                throw new ParseException("В выражении содержатся недопустимые символы: \"" + token +"\"",3);
            }

        }
        while (!stackOperations.empty()) {
            String op = stackOperations.pop();
            if (isOpenBracket(op) || isCloseBracket(op)){
                throw new ParseException("В выражении имеются не закрытые скобки!", 4);
            } else {
                stackRPN.push(op);
            }
        }

        Collections.reverse(stackRPN);
    }

    /**
     * Verifies that the string is a variable
     *
     * @param token for check
     * @return <code>true</code> if variable, otherwise <code>false</code>
     */
    private boolean isNumber(String token) {
        try {
            Double.parseDouble(token);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * Verifies that the string is a integer
     *
     * @param token for check
     * @return <code>true</code> if integer, otherwise <code>false</code>
     */
    private boolean isInteger(String token) {
        return isNumber(token) && token.substring(token.indexOf(".")+1,token.length()).equals("0");
    }

    /**
     * Verifies that the string is a open bracket
     *
     * @param token for check
     * @return <code>true</code> if open bracket, otherwise <code>false</code>
     */
    private boolean isOpenBracket(String token) {
        return token.equals("(");
    }

    /**
     * Verifies that the string is a close bracket
     *
     * @param token for check
     * @return <code>true</code> if close bracket, otherwise <code>false</code>
     */
    private boolean isCloseBracket(String token) {
        return token.equals(")");
    }

    /**
     * Verifies that the string is a operator
     *
     * @param token for check
     * @return <code>true</code> if operator, otherwise <code>false</code>
     */
    private boolean isOperator(String token) {
        return OPERATORS.contains(token);
    }

    /**
     * give a operator precedence
     *
     * @param token for check
     * @return precedence
     */
    private byte getPrecedence(String token) {
        if (token.equals("+") || token.equals("-")) {
            return 1;
        }
        return 2;
    }
}
