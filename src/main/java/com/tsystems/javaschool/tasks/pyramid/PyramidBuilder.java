package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;


public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }
        int pyramydLevelsCount = getCountOfLevels(inputNumbers.size());
        inputNumbers.sort((a, b) -> a - b);
        int[][] pyramid = new int[pyramydLevelsCount][pyramydLevelsCount * 2 - 1];
        int pointer = 0;
        for (int i = 0; i < pyramydLevelsCount; i++) {
            pyramid[i] = getNextString(pyramydLevelsCount * 2 - 1,
                    inputNumbers.subList(pointer, ((i + 1) * (i + 2)) / 2));
            pointer = ((i + 1) * (i + 2)) / 2;
        }
        return pyramid;
    }

    /**
     * Calculate the count of strings for this pyramid
     *
     * @param countOfNumbers in inputNumbers list
     * @return count of strings for this pyramid
     * @throws CannotBuildPyramidException if it's impossible to build pyramide with this count of numbers
     */
    private int getCountOfLevels(int countOfNumbers) throws CannotBuildPyramidException{
        String answer = String.valueOf((Math.sqrt(1 + 8 * countOfNumbers) - 1) / 2.0);
        if (isInteger(answer)) {
            return Integer.parseInt(answer.substring(0,answer.indexOf('.')));
        } else {
            throw new CannotBuildPyramidException();
        }
    }

    /**
     * Calculate the count of zeros to the left and rigth of main part of pyramid in current string
     *
     * @param size width of string
     * @param countOfNumbers count of numbers, that's must be placed in current string
     * @return count of zeros
     */
    private int getCountOfZeroesLR(int size, int countOfNumbers){
        return (size - countOfNumbers * 2 + 1) / 2;
    }

    /**
     * Generate new string for pyramid
     *
     * @param size width of string
     * @param numbers list of numbers, that's must be placed in this string
     * @return <code>int[]</code> next sring for piramid
     */
    private int[] getNextString(int size, List<Integer> numbers){
        int[] nextString = new int[size];
        int countOfZeroesLR = getCountOfZeroesLR(size,numbers.size());
        int pointer = 0;
        for (int i = 0; i < size; i++){
            if (i < countOfZeroesLR || size - i < countOfZeroesLR){
                nextString[i] = 0;
            } else if((i-countOfZeroesLR) % 2 == 0){
                nextString[i] = numbers.get(pointer++);
            } else {
                nextString[i] = 0;
            }
        }
        return nextString;
    }

    /**
     * Verifies that the string is a integer
     *
     * @param s string for check
     * @return <code>true</code> if integer, otherwise <code>false</code>
     */
    private boolean isInteger(String s) {
        return s.substring(s.indexOf(".")+1,s.length()).equals("0");
    }
}
