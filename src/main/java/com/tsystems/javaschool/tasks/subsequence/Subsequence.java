package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        if (x == null || y == null){
            throw new IllegalArgumentException("Последовательность не может быть пустой!");
        }

        int xPointer = 0;
        int yPointer = 0;

        while (yPointer < y.size() && xPointer < x.size()){
            if (y.get(yPointer).equals( x.get(xPointer))){
                xPointer++;
            }
            yPointer++;
        }

        return xPointer == x.size();
    }
}
